<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>String Handle</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        if(!empty($_POST['submit'])) {  
            if(empty($_POST['input'])) {
                echo 'Vui lòng nhập xâu đầu vào';
            }else {
                $input = $_POST['input'];
            }
            if(empty($_POST['handle'])) {
                echo 'Vui lòng nhập cách thức xử lí';
            }else {
                $handle = $_POST['handle'];
            }
        } 
        
    ?>
    <div class="container">
        <div class="form-input">
            <form method="post">
                <input 
                    id="input" 
                    type="text" 
                    name="input" 
                    value="<?php if(isset($input)) echo $input; ?>" 
                    placeholder="Nhập nội dung xâu ký tự"
                ><br>
                <div class="input">
                    <input 
                        type="radio" 
                        name="handle" 
                        value="strlen" 
                        <?php if (isset($handle) && $handle=="strlen") echo "checked";?>
                    >strlen
                    <input 
                        type="radio" 
                        name="handle" 
                        value="strtolower"
                        <?php if (isset($handle) && $handle=="strtolower") echo "checked";?>
                    >strtolower
                    <input 
                        type="radio" 
                        name="handle" 
                        value="trim"
                        <?php if (isset($handle) && $handle=="trim") echo "checked";?>
                    >trim
                    <input 
                        type="radio" 
                        name="handle" 
                        value="strtoupper"
                        <?php if (isset($handle) && $handle=="strtoupper") echo "checked";?>
                    >strtoupper
                </div>
                <br>
                <input class="submit" name="submit" type="submit" value="Submit"><br>
            </form>
            <?php 
                if(!empty($_POST['submit'])) {
                    if(isset($handle) && isset($input)) {
                        if ($handle == 'strlen') {
                            $result = strlen($input);
                        }elseif ($handle == 'strtolower') {
                            $result = strtolower($input);
                        }elseif ($handle == 'trim') {
                            $result = trim($input);
                        }else {
                            $result = strtoupper($input);
                        }
                    } 
                }    
                if(isset($result)) echo "Result: ".$result;
            ?>
        </div>
    </div>
</body>