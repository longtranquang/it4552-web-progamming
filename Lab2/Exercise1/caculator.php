<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Caculator</title>
</head>
<body>
    <?php
        if(!empty($_POST['submit'])) {  
            if(empty($_POST['number1']) || empty($_POST['number2'])) {
                echo 'Vui lòng nhập số';
            }else {
                $number1 = $_POST['number1'];
                $number2 = $_POST['number2'];
            }
            if(empty($_POST['operand'])) {
                echo 'Vui lòng nhập toán hạng';
            }else {
                $operand = $_POST['operand'];
            }
        } 
        
    ?>
    <form method="post">
        <label for="number1">Nhập số thứ 1:</label>
        <input id="number1" type="number" name="number1" value="<?php echo $number1;?>"><br>
        <input 
            type="radio" 
            name="operand" 
            value="plus" 
            <?php if (isset($operand) && $operand=="plus") echo "checked";?>
        >&plus;
        <input 
            type="radio" 
            name="operand" 
            value="minus"
            <?php if (isset($operand) && $operand=="minus") echo "checked";?>
        >&minus;
        <input 
            type="radio" 
            name="operand" 
            value="mul"
            <?php if (isset($operand) && $operand=="mul") echo "checked";?>
        >*
        <input 
            type="radio" 
            name="operand" 
            value="div"
            <?php if (isset($operand) && $operand=="div") echo "checked";?>
        >/
        <br>
        <label for="number2">Nhập số thứ 2:</label>
        <input id="number2" type="number" name="number2" value="<?php echo $number2;?>">
        <br>
        <input name="submit" type="submit" value="Submit"><br>
        
    </form>
    <?php 
        if(!empty($_POST['submit'])) {
            if(isset($operand) && isset($number1) && isset($number2)) {
                if ($operand == 'plus') {
                    $result = $number1 + $number2;
                }elseif ($operand == 'minus') {
                    $result = $number1 - $number2;
                }elseif ($operand == 'mul') {
                    $result = $number1 * $number2;
                }else {
                    $result = $number1 / $number2;
                }
            } 
        }    
        if(isset($result)) echo "<h3>Kết quả: ".$result."</h3>";
    ?>
</body>