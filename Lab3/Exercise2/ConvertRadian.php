<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Convert Radian</title>
</head>
<body>
    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
        <table>
            <tr>
                <td><label for="number">Number</label></td>
                <td><input id="number" name="number" type="number" value="<?php if(isset($POST['number'])) {echo $POST['number'];} ?>"/></td>
            </tr>
            <tr>
                <td><input type="radio" name="pick" value="0">Degrees</td>
                <td><input type="radio" name="pick" value="1">Radian</td>
            </tr>
            <tr>
                <td><input type="submit" name="submit" value="submit"></td>
            </tr>
        </table>
    </form>
    <?php
        const PI = 3.14;
        function toDegree($radian) {
            return $radian * (180/PI);
        }
        function toRadian($degrees) {
            return $degrees * (PI/180);
        }
        if (isset($_POST["submit"]) && isset($_POST["number"]) && isset($_POST["pick"])) {
            $number = $_POST["number"];
            $pick = $_POST["pick"];
            if($pick == "0") {
                $degrees = toDegree($number);
                echo $number." radian convert to degrees: ".$degrees;
            } else {
                $radian = toRadian($number);
                echo $number." degrees convert to radian: ".$radian;
            }
        }
    ?>
</body>
</html>