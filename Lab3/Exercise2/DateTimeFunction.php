<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Date Time Function</title>
</head>
<body>
    <?php 
        if(!empty($_POST["submit"])){
            if(empty($_POST["name1"])){
                print("Please enter person 1's name");
            }
            if(empty($_POST["name1"])){
                print("Please enter person 2's name");
            }
            if(empty($_POST["birthday1"])){
                print("Please enter person 1's birthday");
            }
            if(empty($_POST["birthday2"])){
                print("Please enter person 2's birthday");
            }
        }
    ?>
    <h2>Enter data:</h2>
        <form method="POST" action="<?php echo $_SERVER["PHP_SELF"];?>">
    <table>
        <tr>
            <td>Name1:</td>
            <td><input type="text" name ="name1" value="<?php if(isset($_POST["name1"])) echo $_POST["name1"]; ?>"></td>
        </tr>
        <tr>
            <td>Birthday1:</td>
            <td><input type="date" name ="birthday1" value="<?php if(isset($_POST["birthday1"])) echo $_POST["birthday1"]; ?>"></td>
        </tr> 
        <tr>
            <td>Name2:</td>
            <td><input type="text" name ="name2" value="<?php if(isset($_POST["name2"])) echo $_POST["name2"]; ?>"></td>
        </tr>
        <tr>
            <td>Birthday2:</td>
            <td><input type="date" name ="birthday2" value="<?php if(isset($_POST["birthday2"])) echo $_POST["birthday2"]; ?>"></td>
        </tr>
        <tr>
            <td><input type="SUBMIT" value="SUBMIT"></td>
            <td><input type="RESET" value ="RESET"></td>
        </tr>   
    </table>
    <?php
        if (!empty($_POST["name1"]) && !empty($_POST["name2"]) && !empty($_POST["birthday1"]) && !empty($_POST["birthday2"])) 
        {
            $name1 = $_POST["name1"];
            $birthday1 = $_POST["birthday1"];
            $name2 = $_POST["name2"];
            $birthday2 = $_POST["birthday2"];
            $now = date('d-m-Y');
            $date1 = date_create($birthday1);
            $date2 = date_create($birthday2);
            print  "The difference in days between two dates: ";
            $diff=date_diff($date1,$date2);
            echo $diff->format("%R%a days");
            echo "<br>";
            echo "$name1 :  ";
            echo date("Y") - date("Y",strtotime($birthday1)). " years old";
            echo "<br>";
            echo "$name2 :  ";
            echo date("Y") - date("Y",strtotime($birthday2)). " years old";
            echo "<br>";
            echo "difference years between two persons: ";
            echo "<br>";
            echo date("Y",strtotime($birthday2))-date("Y",strtotime($birthday1))," years";
        }
    ?>
</body>
</html>